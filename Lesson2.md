## Creating Service Accounts

How to create SS imperatively and declaratively. ~ 300 words plus some simple example tasks that walk through creating imperatively and declaratively

The imperative method uses the `kubectl` command blah blah blah. This is where you teach about StatefulSets… up to 500 words in text (no video or audio)

Tasks: Creating StatefulSets
Create a new StatefulSet called `state-app` based on blah blah blah
`$ kubectl create ss state-app….`
more… (text, no video or audio)