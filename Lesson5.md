## Test Yourself

A sample scenario (without instructions, without YAML, and without commands) to test how much users have learned about SS

Test yourself (do not include commands or YAML here, this lesson is to test skills without having the commands in front of you)
1. Create a new StatefulSet called my-app based on the xyzzy image
2. Verify that it’s running
3. Inspect it’s details